package design.patterns.documentation.solid.i;

public interface BearFeeder {
    void feedTheBear();
}
