package design.patterns.documentation.solid.i;

public interface BearKeeper {

    void washTheBear();
    void feedTheBear();
    void petTheBear();

}
