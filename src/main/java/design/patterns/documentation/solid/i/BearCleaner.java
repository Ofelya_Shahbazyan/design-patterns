package design.patterns.documentation.solid.i;

public interface BearCleaner {
    void washTheBear();
}
