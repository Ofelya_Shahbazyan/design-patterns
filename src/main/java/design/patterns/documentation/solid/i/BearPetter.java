package design.patterns.documentation.solid.i;

public interface BearPetter {
    void petTheBear();
}
