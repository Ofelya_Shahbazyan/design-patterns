package design.patterns.documentation.solid.o;

public interface CalculatorOperation {

    void perform();

}
