package design.patterns.documentation.solid.o;

public abstract class AbstractCalculatorOperation {

    abstract void perform();

}
