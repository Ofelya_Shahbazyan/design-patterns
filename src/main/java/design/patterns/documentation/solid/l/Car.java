package design.patterns.documentation.solid.l;

public interface Car {

    void turnOnEngine();
    void accelerate();

}
