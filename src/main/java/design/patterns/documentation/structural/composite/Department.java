package design.patterns.documentation.structural.composite;

public interface Department {

    void printDepartmentName();
}
