package design.patterns.documentation.structural.adapter;

public interface Movable {
    // returns speed in MPH
    double getSpeed();
}
