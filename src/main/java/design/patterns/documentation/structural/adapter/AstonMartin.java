package design.patterns.documentation.structural.adapter;

public class AstonMartin implements Movable {
    @Override
    public double getSpeed() {
        return 220;
    }
}
