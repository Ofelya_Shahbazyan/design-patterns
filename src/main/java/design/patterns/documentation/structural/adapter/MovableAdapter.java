package design.patterns.documentation.structural.adapter;

public interface MovableAdapter {
    // returns speed in KMPH
    double getSpeed();
}
