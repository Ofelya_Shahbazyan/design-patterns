package design.patterns.documentation.structural.adapter;

public class BugattiVeyron implements Movable {
    @Override
    public double getSpeed() {
        return 268;
    }
}
