package design.patterns.documentation.structural.decorator;

public interface ChristmasTree {
    String decorate();
}
