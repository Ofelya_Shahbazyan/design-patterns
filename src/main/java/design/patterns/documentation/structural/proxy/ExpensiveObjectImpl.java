package design.patterns.documentation.structural.proxy;

import static design.patterns.documentation.structural.util.LoggerUtil.LOG;

public class ExpensiveObjectImpl implements ExpensiveObject {

    public ExpensiveObjectImpl() {
        heavyInitialConfiguration();
    }

    @Override
    public void process() {
        LOG.info("processing complete.");
    }

    private void heavyInitialConfiguration() {
        LOG.info("Loading initial configuration..");
    }

}
