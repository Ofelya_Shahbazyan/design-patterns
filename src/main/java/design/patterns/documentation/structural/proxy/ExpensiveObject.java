package design.patterns.documentation.structural.proxy;

public interface ExpensiveObject {
    void process();
}
