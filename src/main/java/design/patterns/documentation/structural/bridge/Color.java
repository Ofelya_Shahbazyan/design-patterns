package design.patterns.documentation.structural.bridge;

public interface Color {
    String fill();
}
