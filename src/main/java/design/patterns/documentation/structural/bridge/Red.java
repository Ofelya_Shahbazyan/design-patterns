package design.patterns.documentation.structural.bridge;

public class Red implements Color {

    @Override
    public String fill() {
        return "Color is Red";
    }

}
