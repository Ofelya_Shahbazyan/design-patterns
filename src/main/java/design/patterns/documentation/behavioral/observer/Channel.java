package design.patterns.documentation.behavioral.observer;

public interface Channel {
    public void update(Object o);
}
