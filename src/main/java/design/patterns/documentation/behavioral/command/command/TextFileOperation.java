package design.patterns.documentation.behavioral.command.command;

@FunctionalInterface
public interface TextFileOperation {

    String execute();

}
