package design.patterns.documentation.behavioral.command.client;

import design.patterns.documentation.behavioral.command.command.OpenTextFileOperation;
import design.patterns.documentation.behavioral.command.command.SaveTextFileOperation;
import design.patterns.documentation.behavioral.command.command.TextFileOperation;
import design.patterns.documentation.behavioral.command.invoker.TextFileOperationExecutor;
import design.patterns.documentation.behavioral.command.receiver.TextFile;

public class TextFileApplication {

    public static void main(String[] args) {

        TextFileOperation openTextFileOperation = new OpenTextFileOperation(new TextFile("file1.txt"));
        TextFileOperation saveTextFileOperation = new SaveTextFileOperation(new TextFile("file2.txt"));
        TextFileOperationExecutor textFileOperationExecutor = new TextFileOperationExecutor();
        System.out.println(textFileOperationExecutor.executeOperation(openTextFileOperation));
        System.out.println(textFileOperationExecutor.executeOperation(saveTextFileOperation));
    }
}
