package design.patterns.documentation.behavioral.nullobject;

public interface Router {

    void route(Message msg);

}
