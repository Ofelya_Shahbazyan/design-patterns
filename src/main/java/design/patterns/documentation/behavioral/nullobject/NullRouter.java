package design.patterns.documentation.behavioral.nullobject;

public class NullRouter implements Router {

    @Override
    public void route(Message msg) {
        // do nothing
    }

}
