package design.patterns.documentation.behavioral.mediator;

public class PowerSupplier {
    public void turnOn() {
        // implementation
    }

    public void turnOff() {
        // implementation
    }
}
