package design.patterns.documentation.behavioral.templatemethod.application;

import design.patterns.documentation.behavioral.templatemethod.model.Computer;
import design.patterns.documentation.behavioral.templatemethod.model.ComputerBuilder;
import design.patterns.documentation.behavioral.templatemethod.model.HighEndComputerBuilder;
import design.patterns.documentation.behavioral.templatemethod.model.StandardComputerBuilder;

public class Application {

    public static void main(String[] args) {
        ComputerBuilder standardComputerBuilder = new StandardComputerBuilder();
        Computer standardComputer = standardComputerBuilder.buildComputer();
        standardComputer.getComputerParts().forEach((k, v) -> System.out.println("Part : " + k + " Value : " + v));

        ComputerBuilder highEndComputerBuilder = new HighEndComputerBuilder();
        Computer highEndComputer = highEndComputerBuilder.buildComputer();
        highEndComputer.getComputerParts().forEach((k, v) -> System.out.println("Part : " + k + " Value : " + v));
    }
}
