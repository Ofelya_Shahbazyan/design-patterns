package design.patterns.documentation.creational.suleymanov.factoryMethod;

public class CppDeveloperImpl implements Developer {
    @Override
    public void writeCode() {
        System.out.println("C++ Developer writes C++ code...");
    }
}
