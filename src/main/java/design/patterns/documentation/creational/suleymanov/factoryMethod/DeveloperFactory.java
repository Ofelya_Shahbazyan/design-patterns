package design.patterns.documentation.creational.suleymanov.factoryMethod;

public interface DeveloperFactory {
    Developer createDeveloper();
}
