package design.patterns.documentation.creational.suleymanov.factoryMethod;

public class Program {

    public static void main(String[] args) {
//        JavaDeveloper javaDeveloper = new JavaDeveloper();
//        javaDeveloper.writeJavaCode();
//
//        CppDeveloper cppDeveloper = new CppDeveloper();
//        cppDeveloper.writeCppCode();

        //

//        Developer javaDeveloper1 = new JavaDeveloperImpl();
//        javaDeveloper1.writeCode();
//
//        Developer cppDeveloper1 = new CppDeveloperImpl();
//        cppDeveloper1.writeCode();

        //

//        DeveloperFactory developerFactory = new JavaDeveloperFactory();
//        Developer developer = developerFactory.createDeveloper();
//        developer.writeCode();
//
//        DeveloperFactory developerFactory1 = new CppDeveloperFactory();
//        Developer developer1 = developerFactory1.createDeveloper();
//        developer1.writeCode();

        //

        DeveloperFactory developerFactory = createDeveloperBySpecialty("Java");
        Developer developer = developerFactory.createDeveloper();
        developer.writeCode();

        DeveloperFactory developerFactory1 = createDeveloperBySpecialty("C++");
        Developer developer1 = developerFactory1.createDeveloper();
        developer1.writeCode();

    }

    static DeveloperFactory createDeveloperBySpecialty(String specialty) {
        if (specialty.equalsIgnoreCase("Java")) {
            return new JavaDeveloperFactory();
        } else if (specialty.equalsIgnoreCase("C++")) {
            return new CppDeveloperFactory();
        } else {
            throw new RuntimeException(specialty + " is unknown specialty.");
        }
    }
}
