package design.patterns.documentation.creational.suleymanov.abstractFactory;

public interface Manager {
    void manageProject();
}
