package design.patterns.documentation.creational.suleymanov.abstractFactory;

public class PhpDeveloper {
    public void writeCode() {
        System.out.println("Php developer writes php code.");
    }
}
