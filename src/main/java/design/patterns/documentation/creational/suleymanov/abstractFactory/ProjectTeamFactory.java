package design.patterns.documentation.creational.suleymanov.abstractFactory;

public interface ProjectTeamFactory {

    Developer getDeveloper();

    Tester getTester();

    Manager getManager();
}
