package design.patterns.documentation.creational.suleymanov.abstractFactory.banking;

import design.patterns.documentation.creational.suleymanov.abstractFactory.Developer;

public class JavaDeveloper implements Developer {
    @Override
    public void writeCode() {
        System.out.println("Java developer writes java code.");
    }
}
