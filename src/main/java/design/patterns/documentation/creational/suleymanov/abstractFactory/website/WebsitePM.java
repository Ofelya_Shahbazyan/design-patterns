package design.patterns.documentation.creational.suleymanov.abstractFactory.website;

import design.patterns.documentation.creational.suleymanov.abstractFactory.Manager;

public class WebsitePM implements Manager {
    @Override
    public void manageProject() {
        System.out.println("Website PM manages project.");
    }
}
