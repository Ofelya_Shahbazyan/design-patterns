package design.patterns.documentation.creational.suleymanov.abstractFactory.website;

import design.patterns.documentation.creational.suleymanov.abstractFactory.Tester;

public class ManualTester implements Tester {
    @Override
    public void testCode() {
        System.out.println("Manual tester tests website.");
    }
}
