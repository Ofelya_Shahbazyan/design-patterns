package design.patterns.documentation.creational.suleymanov.abstractFactory.banking;

import design.patterns.documentation.creational.suleymanov.abstractFactory.Developer;
import design.patterns.documentation.creational.suleymanov.abstractFactory.Manager;
import design.patterns.documentation.creational.suleymanov.abstractFactory.ProjectTeamFactory;
import design.patterns.documentation.creational.suleymanov.abstractFactory.Tester;

public class BankingTeamFactory implements ProjectTeamFactory {
    @Override
    public Developer getDeveloper() {
        return new JavaDeveloper();
    }

    @Override
    public Tester getTester() {
        return new QATester();
    }

    @Override
    public Manager getManager() {
        return new BankingPM();
    }
}
