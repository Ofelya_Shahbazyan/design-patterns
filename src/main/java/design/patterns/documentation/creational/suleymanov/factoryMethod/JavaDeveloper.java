package design.patterns.documentation.creational.suleymanov.factoryMethod;

public class JavaDeveloper {

    public void writeJavaCode() {
        System.out.println("Java Developer writes java code...");
    }
}
