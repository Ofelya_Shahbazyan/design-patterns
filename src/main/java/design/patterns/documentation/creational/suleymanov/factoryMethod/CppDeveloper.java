package design.patterns.documentation.creational.suleymanov.factoryMethod;

public class CppDeveloper {
    public void writeCppCode() {
        System.out.println("C++ Developer writes C++ code...");
    }
}
