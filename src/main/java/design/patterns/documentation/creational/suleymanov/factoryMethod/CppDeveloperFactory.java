package design.patterns.documentation.creational.suleymanov.factoryMethod;

public class CppDeveloperFactory implements DeveloperFactory {
    @Override
    public Developer createDeveloper() {
        return new CppDeveloperImpl();
    }
}
