package design.patterns.documentation.creational.suleymanov.abstractFactory;

public interface Tester {
    void testCode();
}
