package design.patterns.documentation.creational.suleymanov.abstractFactory;

public class ProjectManager {
    public void manageProject() {
        System.out.println("PM manages website project.");
    }
}
