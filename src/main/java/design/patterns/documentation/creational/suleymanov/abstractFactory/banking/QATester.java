package design.patterns.documentation.creational.suleymanov.abstractFactory.banking;

import design.patterns.documentation.creational.suleymanov.abstractFactory.Tester;

public class QATester implements Tester {
    @Override
    public void testCode() {
        System.out.println("QA tester tests banking code.");
    }
}
