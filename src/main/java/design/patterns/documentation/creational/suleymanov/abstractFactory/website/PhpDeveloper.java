package design.patterns.documentation.creational.suleymanov.abstractFactory.website;

import design.patterns.documentation.creational.suleymanov.abstractFactory.Developer;

public class PhpDeveloper implements Developer {
    @Override
    public void writeCode() {
        System.out.println("Php developer writes php code.");
    }
}
