package design.patterns.documentation.creational.suleymanov.abstractFactory;

public interface Developer {
    void writeCode();
}
