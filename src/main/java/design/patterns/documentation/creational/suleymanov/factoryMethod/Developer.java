package design.patterns.documentation.creational.suleymanov.factoryMethod;

public interface Developer {
    void writeCode();
}
