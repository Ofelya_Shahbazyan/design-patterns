package design.patterns.documentation.creational.suleymanov.factoryMethod;

public class JavaDeveloperFactory implements DeveloperFactory {
    @Override
    public Developer createDeveloper() {
        return new JavaDeveloperImpl();
    }
}
