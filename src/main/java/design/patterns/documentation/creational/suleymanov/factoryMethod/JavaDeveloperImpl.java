package design.patterns.documentation.creational.suleymanov.factoryMethod;

public class JavaDeveloperImpl implements Developer {
    @Override
    public void writeCode() {
        System.out.println("Java Developer writes java code...");
    }
}
