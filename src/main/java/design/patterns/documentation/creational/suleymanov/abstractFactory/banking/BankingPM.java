package design.patterns.documentation.creational.suleymanov.abstractFactory.banking;

import design.patterns.documentation.creational.suleymanov.abstractFactory.Manager;

public class BankingPM implements Manager {
    @Override
    public void manageProject() {
        System.out.println("Banking PM manages banking project.");
    }
}
