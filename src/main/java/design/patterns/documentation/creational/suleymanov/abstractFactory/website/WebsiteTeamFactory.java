package design.patterns.documentation.creational.suleymanov.abstractFactory.website;

import design.patterns.documentation.creational.suleymanov.abstractFactory.Developer;
import design.patterns.documentation.creational.suleymanov.abstractFactory.Manager;
import design.patterns.documentation.creational.suleymanov.abstractFactory.ProjectTeamFactory;
import design.patterns.documentation.creational.suleymanov.abstractFactory.Tester;

public class WebsiteTeamFactory implements ProjectTeamFactory {
    @Override
    public Developer getDeveloper() {
        return new PhpDeveloper();
    }

    @Override
    public Tester getTester() {
        return new ManualTester();
    }

    @Override
    public Manager getManager() {
        return new WebsitePM();
    }
}
