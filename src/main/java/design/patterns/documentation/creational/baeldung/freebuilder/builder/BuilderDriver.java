package design.patterns.documentation.creational.baeldung.freebuilder.builder;

public class BuilderDriver {
    public static void main(String[] args) {
        Employee.Builder emplBuilder = new Employee.Builder();

        Employee employee = emplBuilder
                .setName("baeldung")
                .setAge(12)
                .setDepartment("Builder Pattern")
                .build();
        System.out.println(employee.getName() + " " + employee.getDepartment() + " " + employee.getAge());
    }
}
