package design.patterns.documentation.creational.baeldung.singleton.all.synchronization;

/**
 * Enum singleton pattern. Uses an enum to hold a reference to the singleton
 * instance.
 */
public enum EnumSingleton {

    /**
     * Current instance of the singleton.
     */
    INSTANCE;
}
