package design.patterns.documentation.creational.baeldung.factory;

public class Triangle implements Polygon {

    @Override
    public String getType() {
        return "Triangle";
    }

}
