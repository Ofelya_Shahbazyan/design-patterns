package design.patterns.documentation.creational.baeldung.abstractfactory;

public interface Color {
    String getColor();
}
