package design.patterns.documentation.creational.baeldung.factory;

public class Pentagon implements Polygon {

    @Override
    public String getType() {
        return "Pentagon";
    }

}
