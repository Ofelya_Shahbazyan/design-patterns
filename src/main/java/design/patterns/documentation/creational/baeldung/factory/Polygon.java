package design.patterns.documentation.creational.baeldung.factory;

public interface Polygon {
    String getType();
}
