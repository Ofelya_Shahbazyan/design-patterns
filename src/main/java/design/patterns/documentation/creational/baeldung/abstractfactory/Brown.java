package design.patterns.documentation.creational.baeldung.abstractfactory;

public class Brown implements Color {

    @Override
    public String getColor() {
        return "brown";
    }

}
