package design.patterns.documentation.creational.baeldung.abstractfactory;

public interface AbstractFactory<T> {
    T create(String type) ;
}
