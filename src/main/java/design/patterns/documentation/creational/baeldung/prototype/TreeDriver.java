package design.patterns.documentation.creational.baeldung.prototype;

public class TreeDriver {
    public static void main(String[] args) {
        Position position = new Position(4,6);
        PineTree pineTree = new PineTree(5,7);
        PlasticTree plasticTree = new PlasticTree(3,9);
        pineTree.setPosition(position);
        plasticTree.setPosition(position);

        System.out.println(pineTree.getType() + " " + pineTree.copy());
        System.out.println(plasticTree.getName() + " " + plasticTree.copy());

    }
}
