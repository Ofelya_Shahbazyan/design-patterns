package design.patterns.documentation.creational.baeldung.abstractfactory;

public interface Animal {
    String getType();
    String makeSound();
}
