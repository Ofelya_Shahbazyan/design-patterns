package design.patterns.documentation.creational.baeldung.factory;

public class Heptagon implements Polygon {

    @Override
    public String getType() {
        return "Heptagon";
    }

}
