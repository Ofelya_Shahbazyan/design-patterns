package design.patterns.documentation.creational.baeldung.abstractfactory;

public class White implements Color {

    @Override
    public String getColor() {
        return "White";
    }

}
