package design.patterns.documentation.behavioral.command;

import design.patterns.documentation.behavioral.command.command.SaveTextFileOperation;
import design.patterns.documentation.behavioral.command.command.TextFileOperation;
import design.patterns.documentation.behavioral.command.receiver.TextFile;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SaveTextFileOperationUnitTest {

    @Test
    public void givenSaveTextFileOperationIntance_whenCalledExecuteMethod_thenOneAssertion() {
        TextFileOperation openTextFileOperation = new SaveTextFileOperation(new TextFile("file1.txt"));
        assertThat(openTextFileOperation.execute()).isEqualTo("Saving file file1.txt");
    }
}
