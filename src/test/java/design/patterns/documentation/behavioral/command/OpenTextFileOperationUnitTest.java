package design.patterns.documentation.behavioral.command;

import design.patterns.documentation.behavioral.command.command.OpenTextFileOperation;
import design.patterns.documentation.behavioral.command.command.TextFileOperation;
import design.patterns.documentation.behavioral.command.receiver.TextFile;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class OpenTextFileOperationUnitTest {

    @Test
    public void givenOpenTextFileOperationIntance_whenCalledExecuteMethod_thenOneAssertion() {
        TextFileOperation openTextFileOperation = new OpenTextFileOperation(new TextFile("file1.txt"));
        assertThat(openTextFileOperation.execute()).isEqualTo("Opening file file1.txt");
    }
}
